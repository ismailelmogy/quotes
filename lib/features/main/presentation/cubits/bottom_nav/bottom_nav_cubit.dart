import 'dart:collection';
import 'package:flutter_bloc/flutter_bloc.dart';
part 'bottom_nav_state.dart';


class BottomNavCubit extends Cubit<BottomNavState> {
  BottomNavCubit() : super(BottomNavInitial());

  int bottomNavIndex = 0;

  // List<Widget> bottomNavScreens = [
  //   const MenuScreen(),
  //   const ChatScreen(),
  //   const ToDoScreen(),
  //   const ProfileScreen(),
  
  // ];

  // Widget get selectedBottomNavScreen => bottomNavScreens[bottomNavIndex];

  ListQueue<int> navigationQueue = ListQueue();

  void upadateBottomNavIndex(int value) {
    emit(BottomNavInitial());
    bottomNavIndex = value;
    emit(BottomNavIsChanging());
  }

}
