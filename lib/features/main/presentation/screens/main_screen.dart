import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:quotesapp/config/themes/cubit/theme_cubit.dart';
import 'package:quotesapp/core/utils/app_colors.dart';
import 'package:quotesapp/core/utils/text_styles.dart';
import 'package:quotesapp/core/widgets/loading_indicator.dart';
import 'package:quotesapp/core/widgets/page_container.dart';
import 'package:quotesapp/features/favorite_quotes/presentation/cubit/favorite_quotes_cubit.dart';
import 'package:quotesapp/features/favorite_quotes/presentation/screens/favorite_quotes.dart';
import 'package:quotesapp/features/main/presentation/cubits/bottom_nav/bottom_nav_cubit.dart';
import 'package:quotesapp/features/quotes/presentation/cubit/quote_cubit.dart';
import 'package:quotesapp/features/quotes/presentation/screens/quotes.dart';
import 'package:quotesapp/injection_container.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  List<Widget> screens = [
    const QuotesScreen(key: ValueKey("quotes")),
    const FavoriteQuotesScreen(key: ValueKey("fav_quotes")),
  ];

  List<BottomNavigationBarItem> navigationBarItems = [
    const BottomNavigationBarItem(
      icon: Icon(
        IconlyBold.home,
      ),
      backgroundColor: Colors.transparent,
      label: 'Home',
    ),
    const BottomNavigationBarItem(
      icon: Icon(
        IconlyBold.heart,
      ),
      backgroundColor: Colors.transparent,
      label: 'Saved',
    ),
  ];

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Theme.of(context).backgroundColor),
      );
    });
  }

  void _onItemTapped(int index) {
    BlocProvider.of<BottomNavCubit>(context)
        .navigationQueue
        .addLast(BlocProvider.of<BottomNavCubit>(context).bottomNavIndex);
    BlocProvider.of<BottomNavCubit>(context).upadateBottomNavIndex(index);
  }

  @override
  Widget build(BuildContext context) {
    return PageContainer(child:
        BlocBuilder<BottomNavCubit, BottomNavState>(builder: (context, state) {
      return WillPopScope(
          onWillPop: () async {
            if (BlocProvider.of<BottomNavCubit>(context)
                .navigationQueue
                .isEmpty) return true;
            BlocProvider.of<BottomNavCubit>(context).upadateBottomNavIndex(
                BlocProvider.of<BottomNavCubit>(context).navigationQueue.last);
            BlocProvider.of<BottomNavCubit>(context)
                .navigationQueue
                .removeLast();
            return false;
          },
          child: Scaffold(
              appBar: PreferredSize(
                preferredSize: const Size.fromHeight(0.0),
                child: AppBar(
                  backgroundColor: Theme.of(context).backgroundColor,
                  elevation: 0.0,
                ),
              ),
              body: ListView(
                physics: const BouncingScrollPhysics(),
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 24.0,
                      right: 24,
                      top: 16.0,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 35,
                          width: 35,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: accentColor,
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Text(
                            "d",
                            style: textStyle.apply(
                              color: Colors.white,
                              fontSizeDelta: 3,
                              fontWeightDelta: 5,
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: () => context
                              .read<ThemeCubit>()
                              .updateAppTheme(
                                  context.read<ThemeCubit>().currentThemeMode ==
                                          ThemeMode.dark
                                      ? ThemeMode.light
                                      : ThemeMode.dark),
                          icon: Icon(
                            context.read<ThemeCubit>().currentThemeMode ==
                                    ThemeMode.dark
                                ? Icons.light_mode
                                : Icons.dark_mode,
                            size: 20,
                          ),
                          color: Theme.of(context).iconTheme.color,
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  MultiBlocProvider(
                    providers: [
                      BlocProvider(
                        create: (_) => sl<QuoteCubit>(),
                      ),
                      BlocProvider<FavoriteQuotesCubit>(
                        create: (_) => sl<FavoriteQuotesCubit>(),
                      ),
                    ],
                    child:
                        BlocConsumer<FavoriteQuotesCubit, FavoriteQuotesState>(
                      listener: (context, state) {
                        if (state is FavoriteQuoteIsAddedSuccessfully) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(state.message),
                          ));
                        } else if (state is FailedtoAddFavoriteQuote) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(state.message),
                          ));
                        }
                      },
                      builder: (context, state) {
                        return Stack(
                          children: [
                            SizedBox(
                              height: MediaQuery.of(context).size.height - 100,
                              child: screens.elementAt(context
                                  .watch<BottomNavCubit>()
                                  .bottomNavIndex),
                            ),
                            context.watch<FavoriteQuotesCubit>().isLoadingForAdd
                                ? const Positioned.fill(
                                    child: Align(
                                        alignment: Alignment.center,
                                        child: LoadingIndicator()))
                                : const SizedBox(),
                          ],
                        );
                      },
                    ),
                  )
                ],
              ),
              bottomNavigationBar: BottomNavigationBar(
                elevation: 0.0,
                type: BottomNavigationBarType.fixed,
                backgroundColor: Theme.of(context).backgroundColor,
                showSelectedLabels: false,
                showUnselectedLabels: false,
                enableFeedback: true,
                iconSize: 24,
                items: navigationBarItems,
                currentIndex: context.watch<BottomNavCubit>().bottomNavIndex,
                selectedItemColor: Theme.of(context).colorScheme.secondary,
                unselectedItemColor: Theme.of(context).unselectedWidgetColor,
                onTap: _onItemTapped,
              )));
    }));
  }
}
