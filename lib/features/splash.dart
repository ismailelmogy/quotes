import 'dart:async';
import 'package:flutter/material.dart';
import 'package:quotesapp/config/locale/app_localizations.dart';
import 'package:quotesapp/core/utils/app_colors.dart';
import 'package:quotesapp/core/utils/constants.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late Animation<double> _opacityAnimation;
  late AnimationController _animationController;
  late Animation<double> _translateY;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 2200), vsync: this);
    _translateY = Tween(begin: -0.12, end: 0.0).animate(
      CurvedAnimation(curve: Curves.easeInOut, parent: _animationController),
    );
    _opacityAnimation = Tween(begin: 0.1, end: 1.0).animate(
      CurvedAnimation(parent: _animationController, curve: Curves.easeInOut),
    );

    _animationController.forward();
    _startTimeout();
  }

  void _startTimeout() {
    Timer(const Duration(milliseconds: 4500),
        () => {Navigator.pushReplacementNamed(context, mainRoute)});
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/splash.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  tileMode: TileMode.clamp,
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    accentColor.withOpacity(.08),
                    Colors.black.withOpacity(.56)
                  ]),
            ),
          ),
          AnimatedBuilder(
            animation: _animationController,
            builder: (context, widget) {
              return Center(
                child: Transform(
                  transform: Matrix4.translationValues(
                      0,
                      _translateY.value * MediaQuery.of(context).size.height,
                      0),
                  alignment: Alignment.center,
                  child: FadeTransition(
                    opacity: _opacityAnimation,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        const SizedBox(height: 150),
                        Material(
                          type: MaterialType.transparency,
                          child: Text(
                            AppLocalizations.of(context)!
                                .translate('app_name')!,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 38,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
