import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:quotesapp/core/error/failures.dart';
import 'package:quotesapp/core/usecases/usecase.dart';
import 'package:quotesapp/features/favorite_quotes/domain/repositories/favorite_quotes_repository.dart';
import 'package:quotesapp/features/quotes/data/models/quote_model.dart';

class LikeQuote implements UseCase<bool, LikeParams> {
  final FavoriteQuotesRepository favoriteQuotesRepository;

  LikeQuote({required this.favoriteQuotesRepository});

  @override
  Future<Either<Failure, bool>> call(LikeParams params) async =>
     await favoriteQuotesRepository.addQuoteToFavList(params.quote);
  
}

class LikeParams extends Equatable {
  final QuoteModel quote;
  
  const LikeParams({required this.quote});

  @override
  List<Object> get props => [quote];
}
