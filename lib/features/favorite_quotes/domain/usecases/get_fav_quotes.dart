import 'package:dartz/dartz.dart';
import 'package:quotesapp/core/error/failures.dart';
import 'package:quotesapp/core/usecases/usecase.dart';
import 'package:quotesapp/features/favorite_quotes/domain/repositories/favorite_quotes_repository.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';

class GetFavQuotes implements UseCase<List<Quote>, NoParams> {
  final FavoriteQuotesRepository favQuotesRepository;

  GetFavQuotes({required this.favQuotesRepository});

  @override
  Future<Either<Failure, List<Quote>>> call(NoParams params) async {
    return await favQuotesRepository.getFavQuotes();
  }
}


