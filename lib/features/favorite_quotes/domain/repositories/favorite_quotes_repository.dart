import 'package:quotesapp/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:quotesapp/features/quotes/data/models/quote_model.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';

abstract class FavoriteQuotesRepository {
  Future<Either<Failure, bool>> removeQuoteFromFavList(int id);
  Future<Either<Failure, bool>> addQuoteToFavList(QuoteModel quote);
   Future<Either<Failure, List<Quote>>> getFavQuotes();
}
