import 'package:dartz/dartz.dart';
import 'package:quotesapp/core/error/exceptions.dart';
import 'package:quotesapp/core/error/failures.dart';
import 'package:quotesapp/features/favorite_quotes/data/datasources/favorite_quotes_local_data_source.dart';
import 'package:quotesapp/features/favorite_quotes/domain/repositories/favorite_quotes_repository.dart';
import 'package:quotesapp/features/quotes/data/models/quote_model.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';

class FavoriteQuotesRepositoryImpl implements FavoriteQuotesRepository {
  final FavoriteQuotesLocalDataSource favoriteQuotesLocalDataSource;

  FavoriteQuotesRepositoryImpl({
    required this.favoriteQuotesLocalDataSource,
  });

  @override
  Future<Either<Failure, bool>> addQuoteToFavList(QuoteModel quote) async {
    try {
      final response =
          await favoriteQuotesLocalDataSource.addQuoteToFavList(quote);
      return Right(response);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> removeQuoteFromFavList(int id) async {
    try {
      final response =
          await favoriteQuotesLocalDataSource.removeQuoteFromFavList(id);
      return Right(response);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, List<Quote>>> getFavQuotes() async {
    try {
      final response = await favoriteQuotesLocalDataSource.getFavQuotes();
      return Right(response);
    } on CacheException {
      return Left(CacheFailure());
    }
  }
}
