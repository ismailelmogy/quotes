part of 'favorite_quotes_cubit.dart';

abstract class FavoriteQuotesState extends Equatable {
  const FavoriteQuotesState();

  @override
  List<Object> get props => [];
}

class FavoriteQuotesInitial extends FavoriteQuotesState {}

class FavoriteQuotesIsLoading extends FavoriteQuotesState {
  final bool isLoading;

  const FavoriteQuotesIsLoading({required this.isLoading});
}

class FavoriteQuotesLoaded extends FavoriteQuotesState {
  final List<Quote> quotes;
  const FavoriteQuotesLoaded({required this.quotes, quote});

  @override
  List<Object> get props => [quotes];
}

class FavoriteQuoteIsAddedSuccessfully extends FavoriteQuotesState {
  final String message;
  const FavoriteQuoteIsAddedSuccessfully({required this.message});

  @override
  List<Object> get props => [message];
}

class FailedtoAddFavoriteQuote extends FavoriteQuotesState {
  final String message;
  const FailedtoAddFavoriteQuote({required this.message});

  @override
  List<Object> get props => [message];
}

class FavoriteQuoteIsDeletedSuccessfully extends FavoriteQuotesState {
  final String message;
  const FavoriteQuoteIsDeletedSuccessfully({required this.message});

  @override
  List<Object> get props => [message];
}

class FailedtoDeleteFavoriteQuote extends FavoriteQuotesState {
  final String message;
  const FailedtoDeleteFavoriteQuote({required this.message});

  @override
  List<Object> get props => [message];
}

class FavoriteQuotesError extends FavoriteQuotesState {
  final String message;
  const FavoriteQuotesError({required this.message});

  @override
  List<Object> get props => [message];
}
