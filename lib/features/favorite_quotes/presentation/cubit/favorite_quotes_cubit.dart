import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:quotesapp/core/error/failures.dart';
import 'package:quotesapp/core/usecases/usecase.dart';
import 'package:quotesapp/core/utils/constants.dart';
import 'package:quotesapp/features/favorite_quotes/domain/usecases/delete_quote.dart';
import 'package:quotesapp/features/favorite_quotes/domain/usecases/get_fav_quotes.dart';
import 'package:quotesapp/features/favorite_quotes/domain/usecases/like_quote.dart';
import 'package:quotesapp/features/quotes/data/models/quote_model.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';
part 'favorite_quotes_state.dart';

class FavoriteQuotesCubit extends Cubit<FavoriteQuotesState> {
  final LikeQuote likeQuote;
  final DeleteQuote deleteQuote;
  final GetFavQuotes getFavQuotes;
  FavoriteQuotesCubit(
      {required this.likeQuote,
      required this.deleteQuote,
      required this.getFavQuotes})
      : super(FavoriteQuotesInitial());
  bool isLoadingForAdd = false, isLoading = false;

  Future<void> addQuoteToFavQuotes(QuoteModel quote) async {
    _changeLoadingViewForAddQuote();
    Either<Failure, bool> response = await likeQuote(LikeParams(quote: quote));
    _changeLoadingViewForAddQuote();
    emit(response
        .fold((failure) => const FavoriteQuotesError(message: cacheFailureMsg),
            (value) {
      if (value == true) {
        return const FavoriteQuoteIsAddedSuccessfully(
            message: addFavSuccessMsg);
      } else {
        return const FailedtoAddFavoriteQuote(message: addFavFaliureMsg);
      }
    }));
  }

  Future<void> deleteQuoteFromFavQuotes(int id) async {
    _changeLoadingView();
    Either<Failure, bool> response = await deleteQuote(Params(id: id));
    _changeLoadingView();
    emit(response
        .fold((failure) => const FavoriteQuotesError(message: cacheFailureMsg),
            (value) {
      if (value == true) {
        return const FavoriteQuoteIsDeletedSuccessfully(
            message: deleteFavSuccessMsg);
      } else {
        return const FailedtoDeleteFavoriteQuote(message: deleteFavFaliureMsg);
      }
    }));
    getFavQuoteList();
  }

  void _changeLoadingViewForAddQuote() {
    isLoadingForAdd = !isLoadingForAdd;
    emit(FavoriteQuotesIsLoading(isLoading: isLoadingForAdd));
  }

  void _changeLoadingView() {
    isLoading = !isLoading;
    emit(FavoriteQuotesIsLoading(isLoading: isLoading));
  }

  Future<void> getFavQuoteList() async {
    _changeLoadingView();
    Either<Failure, List<Quote>> response = await getFavQuotes.call(NoParams());
    _changeLoadingView();
    emit(response
        .fold((failure) => const FavoriteQuotesError(message: cacheFailureMsg),
            (quotes) {
      return FavoriteQuotesLoaded(quotes: quotes);
    }));
  }
}
