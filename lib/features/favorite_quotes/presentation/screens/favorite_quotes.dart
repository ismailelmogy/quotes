import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jiffy/jiffy.dart';
import 'package:quotesapp/config/themes/cubit/theme_cubit.dart';
import 'package:quotesapp/core/utils/constants.dart';
import 'package:quotesapp/core/utils/text_styles.dart';
import 'package:quotesapp/core/widgets/color_loader.dart';
import 'package:quotesapp/features/favorite_quotes/presentation/cubit/favorite_quotes_cubit.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';
import 'package:quotesapp/core/widgets/error.dart';
import 'package:quotesapp/features/quotes/presentation/widgets/quote_card.dart';

class FavoriteQuotesScreen extends StatefulWidget {
  const FavoriteQuotesScreen({Key? key}) : super(key: key);

  @override
  _FavoriteQuotesScreenState createState() => _FavoriteQuotesScreenState();
}

class _FavoriteQuotesScreenState extends State<FavoriteQuotesScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<FavoriteQuotesCubit>(context).getFavQuoteList();
  }

  Widget _buildBody({required List<Quote> quotes}) {
    String currentTheme =
        context.read<ThemeCubit>().currentThemeMode == ThemeMode.dark
            ? 'dark'
            : 'light';

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  Jiffy().yMMMEd.toString(),
                  style: textStyle.apply(
                    color: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.color
                        ?.withOpacity(
                          .5,
                        ),
                    fontSizeDelta: -4,
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  favQuotesScreenSubtitle,
                  style: textStyle.apply(
                    color: Theme.of(context).textTheme.bodyText1?.color,
                    fontWeightDelta: 5,
                    fontSizeDelta: 8,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          quotes.isEmpty
              ? Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 24,
                      ),
                      Image.asset(
                        "assets/images/empty_$currentTheme.png",
                        height: 250,
                        // width: 100,
                        fit: BoxFit.contain,
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      Text(
                        "No saved quotes",
                        textAlign: TextAlign.center,
                        style: textStyle.apply(
                          color: Theme.of(context).textTheme.bodyText1?.color,
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  height: MediaQuery.of(context).size.height - 200,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: ListView.builder(
                      itemCount: quotes.length,
                      itemBuilder: (context, index) {
                        return QuoteCard(
                            isFavScreen: true,
                            quote: quotes[index],
                            deleteQuote: () {
                              BlocProvider.of<FavoriteQuotesCubit>(context)
                                  .deleteQuoteFromFavQuotes(quotes[index].id!);
                            });
                      }),
                ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<FavoriteQuotesCubit, FavoriteQuotesState>(
      listener: (context, state) {
        if (state is FavoriteQuoteIsDeletedSuccessfully) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(state.message),
          ));
        } else if (state is FailedtoDeleteFavoriteQuote) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(state.message),
          ));
        }
      },
      builder: (context, state) {
        if (state is FavoriteQuotesIsLoading) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              Center(
                  child: ColorLoader(
                radius: 20.0,
                dotRadius: 5.0,
              )),
            ],
          );
        } else if (state is FavoriteQuotesLoaded) {
          return _buildBody(quotes: state.quotes);
        } else if (state is FavoriteQuotesError) {
          return Error(
            errorMessage: state.message,
          );
        }
        return const Center(
            child: ColorLoader(
          radius: 25.0,
          dotRadius: 6.0,
        ));
      },
    );
  }
}
