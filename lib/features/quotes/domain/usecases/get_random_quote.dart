import 'package:dartz/dartz.dart';
import 'package:quotesapp/core/error/failures.dart';
import 'package:quotesapp/core/usecases/usecase.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';
import 'package:quotesapp/features/quotes/domain/repositories/quote_repository.dart';


class GetRandomQuote implements UseCase<Quote, NoParams> {
  final QuoteRepository quoteRepository;

  GetRandomQuote({required this.quoteRepository});

  @override
  Future<Either<Failure, Quote>> call(NoParams params) async {
    return await quoteRepository.getRandomQuote();
  }
}