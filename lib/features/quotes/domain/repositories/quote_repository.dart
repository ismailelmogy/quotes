import 'package:quotesapp/core/error/failures.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';
import 'package:dartz/dartz.dart';

abstract class QuoteRepository {
 Future<Either<Failure, Quote>> getRandomQuote();
}