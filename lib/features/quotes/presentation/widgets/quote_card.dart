import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:quotesapp/config/themes/cubit/theme_cubit.dart';
import 'package:quotesapp/core/utils/app_colors.dart';
import 'package:quotesapp/core/utils/text_styles.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';
import 'package:tinycolor2/tinycolor2.dart';
import 'package:share_plus/share_plus.dart';

class QuoteCard extends StatelessWidget {
  final Quote quote;
  final bool isFavScreen;
  final VoidCallback? likeQuote;
  final VoidCallback? onReload;
  final VoidCallback? deleteQuote;
  const QuoteCard(
      {Key? key,
      required this.quote,
      this.isFavScreen = false,
      this.onReload,
      this.likeQuote,
      this.deleteQuote})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        height: 300,
        padding: const EdgeInsets.symmetric(
          vertical: 12,
          horizontal: 18,
        ),
        decoration: BoxDecoration(
          color: context.read<ThemeCubit>().currentThemeMode == ThemeMode.dark
              ? TinyColor(Theme.of(context).backgroundColor).color.tint(5)
              : TinyColor(Theme.of(context).backgroundColor).color.lighten(15),
          borderRadius: BorderRadius.circular(
            20,
          ),
          border: Border.all(
            color: context.read<ThemeCubit>().currentThemeMode == ThemeMode.dark
                ? whiteBackgroundColor.withOpacity(.09)
                : screenBackgroundColor.withOpacity(.008),
            width: .8,
          ),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 5),
              blurRadius: 10,
              color:
                  context.read<ThemeCubit>().currentThemeMode == ThemeMode.dark
                      ? whiteBackgroundColor.withOpacity(.02)
                      : screenBackgroundColor.withOpacity(.02),
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const SizedBox(
              height: 16,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Text(
                  quote.quote!,
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 10,
                  style: textStyle.apply(
                    fontSizeDelta: 4,
                    fontWeightDelta: 6,
                    color: Theme.of(context).textTheme.bodyText1?.color,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              "By " + quote.author!,
              style: textStyle.apply(
                color: Theme.of(context)
                    .textTheme
                    .bodyText1
                    ?.color
                    ?.withOpacity(.8),
                fontSizeDelta: -2,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                IconButton(
                  onPressed: () {
                    Share.share(quote.quote!);
                  },
                  color: Theme.of(context).iconTheme.color,
                  icon: const Icon(
                    Icons.share,
                    size: 24,
                  ),
                ),
                const Spacer(),
                !isFavScreen
                    ? IconButton(
                        onPressed: () => onReload!(),
                        color: Theme.of(context).iconTheme.color,
                        icon: const Icon(
                          Icons.refresh,
                          size: 28,
                        ),
                      )
                    : const SizedBox(),
                const Spacer(),
                !isFavScreen
                    ? IconButton(
                        onPressed: () => likeQuote!(),
                        color: Theme.of(context).iconTheme.color,
                        icon: const Icon(
                          FontAwesomeIcons.heart,
                          size: 22,
                        ),
                      )
                    : IconButton(
                        onPressed: () => deleteQuote!(),
                        color: Theme.of(context).iconTheme.color,
                        icon: const Icon(
                          FontAwesomeIcons.trash,
                          size: 22,
                        ),
                      ),
              ],
            ),
          ],
        ),
      );
}
