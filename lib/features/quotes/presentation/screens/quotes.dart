import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jiffy/jiffy.dart';
import 'package:quotesapp/core/utils/constants.dart';
import 'package:quotesapp/core/utils/text_styles.dart';
import 'package:quotesapp/core/widgets/color_loader.dart';
import 'package:quotesapp/core/widgets/error.dart';
import 'package:quotesapp/features/favorite_quotes/presentation/cubit/favorite_quotes_cubit.dart';
import 'package:quotesapp/features/quotes/data/models/quote_model.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';
import 'package:quotesapp/features/quotes/presentation/cubit/quote_cubit.dart';
import 'package:quotesapp/features/quotes/presentation/widgets/quote_card.dart';

class QuotesScreen extends StatefulWidget {
  const QuotesScreen({Key? key}) : super(key: key);

  @override
  _QuotesScreenState createState() => _QuotesScreenState();
}

class _QuotesScreenState extends State<QuotesScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<QuoteCubit>(context).getQuoteForRandom();
  }

  Widget _buildBody({required Quote quote}) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  Jiffy().yMMMEd.toString(),
                  style: textStyle.apply(
                    color: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.color
                        ?.withOpacity(
                          .5,
                        ),
                    fontSizeDelta: -4,
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  quotesScreenSubtitle,
                  style: textStyle.apply(
                    color: Theme.of(context).textTheme.bodyText1?.color,
                    fontWeightDelta: 5,
                    fontSizeDelta: 8,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 300,
              padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
            child:
                 QuoteCard(
                    quote: quote,
                    likeQuote: () =>
                        BlocProvider.of<FavoriteQuotesCubit>(context)
                            .addQuoteToFavQuotes(QuoteModel(
                                author: quote.author!,
                                quote: quote.quote,
                                permalink: quote.permalink,
                                id: quote.id)),
                    onReload: () => BlocProvider.of<QuoteCubit>(context)
                        .getQuoteForRandom())),
          
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QuoteCubit, QuoteState>(
      builder: (context, state) {
        if (state is QuoteIsLoading) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              Center(
                  child: ColorLoader(
                radius: 20.0,
                dotRadius: 5.0,
              )),
            ],
          );
        } else if (state is QuoteLoaded) {
          return _buildBody(quote: state.quote);
        } else if (state is QuoteError) {
          return Error(
            errorMessage: state.message,
          );
        }
        return const Center(
            child: ColorLoader(
          radius: 25.0,
          dotRadius: 6.0,
        ));
      },
    );
  }
}
