import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:quotesapp/core/error/failures.dart';
import 'package:quotesapp/core/usecases/usecase.dart';
import 'package:quotesapp/core/utils/constants.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';
import 'package:quotesapp/features/quotes/domain/usecases/get_random_quote.dart';

part 'quote_state.dart';

class QuoteCubit extends Cubit<QuoteState> {
  final GetRandomQuote getRandomQuote;
  QuoteCubit({
    required this.getRandomQuote,
  }) : super(QuoteInitial());

  Future<void> getQuoteForRandom() async {
    emit(QuoteIsLoading());
    Either<Failure, Quote> response = await getRandomQuote.call(NoParams());
    emit(response.fold(
        (failure) => QuoteError(message: _mapFailureToMessage(failure)),
        (quote) => QuoteLoaded(quote: quote)));
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMsg;
      case CacheFailure:
        return cacheFailureMsg;
      default:
        return 'Unexpected error';
    }
  }
}
