import 'package:dartz/dartz.dart';
import 'package:quotesapp/core/error/exceptions.dart';
import 'package:quotesapp/core/error/failures.dart';
import 'package:quotesapp/core/network/network_info.dart';
import 'package:quotesapp/features/quotes/data/datasources/quote_local_data_source.dart';
import 'package:quotesapp/features/quotes/data/datasources/quote_remote_data_source.dart';
import 'package:quotesapp/features/quotes/domain/entities/quote.dart';
import 'package:quotesapp/features/quotes/domain/repositories/quote_repository.dart';

class QuoteRepositoryImpl implements QuoteRepository {
  final QuoteRemoteDataSource quoteRemoteDataSource;
  final QuoteLocalDataSource quoteLocalDataSource;
  final NetworkInfo networkInfo;

  QuoteRepositoryImpl({
    required this.quoteRemoteDataSource,
    required this.quoteLocalDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, Quote>> getRandomQuote() async {
    if (await networkInfo.isConnected) {
      try {
        final remoteQuote = await quoteRemoteDataSource.getRandomQuote();
        quoteLocalDataSource.cacheQuote(remoteQuote);
        return Right(remoteQuote);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localQuote = await quoteLocalDataSource.getLastQuote();
        return Right(localQuote);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
