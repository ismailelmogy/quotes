import 'package:quotesapp/core/api/http_consumer.dart';
import 'package:quotesapp/core/utils/constants.dart';
import 'package:quotesapp/features/quotes/data/models/quote_model.dart';

abstract class QuoteRemoteDataSource {
  Future<QuoteModel> getRandomQuote();
}

class QuoteRemoteDataSourceImpl implements QuoteRemoteDataSource {
  final HttpConsumer httpConsumer;
  QuoteRemoteDataSourceImpl({required this.httpConsumer});

  @override
  Future<QuoteModel> getRandomQuote() async {
    final responseJson = await httpConsumer.get(randomQuoteUrl);
    return QuoteModel.fromJson(responseJson);
  }
}
