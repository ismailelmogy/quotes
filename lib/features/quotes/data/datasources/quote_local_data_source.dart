import 'dart:convert';
import 'package:quotesapp/core/error/exceptions.dart';
import 'package:quotesapp/core/utils/constants.dart';
import 'package:quotesapp/features/quotes/data/models/quote_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class QuoteLocalDataSource {
  Future<QuoteModel> getLastQuote();

  Future<void> cacheQuote(QuoteModel quoteToCache);
}

class QuoteLocalDataSourceImpl implements QuoteLocalDataSource {
  final SharedPreferences sharedPreferences;

  QuoteLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<QuoteModel> getLastQuote() {
    final jsonString = sharedPreferences.getString(cachedQuote);
    if (jsonString != null) {
      return Future.value(QuoteModel.fromJson(json.decode(jsonString)));
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> cacheQuote(QuoteModel quoteToCache) {
    return sharedPreferences.setString(
      cachedQuote,
      json.encode(quoteToCache.toJson()),
    );
  }
}
