import 'package:quotesapp/features/quotes/domain/entities/quote.dart';

class QuoteModel extends Quote {
  const QuoteModel({
    required String author,
    required String? quote,
    required String? permalink,
    required int? id,
  }) : super(author: author, quote: quote, permalink: permalink, id: id);

  factory QuoteModel.fromJson(Map<String, dynamic> json) => QuoteModel(
        author: json["author"],
        id: json["id"],
        quote: json["quote"],
        permalink: json["permalink"],
      );

  Map<String, dynamic> toJson() => {
        "author": author,
        "id": id,
        "quote": quote,
        "permalink": permalink,
      };

  // Convert a Quote object into a Map object
  Map<String, dynamic> toDbMap() {
    var map = <String, dynamic>{};
    if (id != null) {
      map['id'] = id;
    }
    map['author'] = author;
    map['quote'] = quote;
    map['permalink'] = permalink;

    return map;
  }
}
