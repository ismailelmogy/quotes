import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:quotesapp/injection_container.dart';
import 'api_consumer.dart';
import 'dart:io';
import 'dart:async';
import 'custom_exception.dart';
import 'dio_response.dart';

class DioConsumer extends ApiConsumer {
  @override
  Future<dynamic> get(String url, {Map<String, String>? headers}) async {
    try {
      Dio dio = sl<Dio>();
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };
      final response = await dio.get(url,
          options: Options(
             contentType: 'application/json',
            responseType: ResponseType.plain,
            followRedirects: false,
            validateStatus: (status) {
              return status! < 500;
            },
            headers: headers 
           
          )); 
      return handleDioResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
  }

  @override
  Future<dynamic> post(String url,
      {body, Map<String, String>? headers}) async {
    try {
  Dio dio = sl<Dio>();
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };
      final response = await dio.post(url,
          data: body,
          options: Options(
            contentType: 'application/json',
            responseType: ResponseType.plain,
            followRedirects: false,
            validateStatus: (status) {
              return status! < 500;
            },
            headers: headers 
          ));
      var responseJson = handleDioResponse(response);
      return responseJson;
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
  }

    @override
  Future<dynamic> put(String url,
      {body, Map<String, String>? headers}) async {
    try {
  Dio dio = sl<Dio>();
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };
      final response = await dio.put(url,
          data: body,
          options: Options(
            contentType: 'application/json',
            responseType: ResponseType.plain,
            followRedirects: false,
            validateStatus: (status) {
              return status! < 500;
            },
            headers: headers ,
          ));
      var responseJson = handleDioResponse(response);
      return responseJson;
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
  }
}
