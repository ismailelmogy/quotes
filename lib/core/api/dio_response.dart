import 'dart:convert';
import 'package:dio/dio.dart';
import 'custom_exception.dart';

dynamic handleDioResponse(Response<dynamic> response) {
  switch (response.statusCode) {
    case 500:
      throw FetchDataException('Error occured while Communication with Server');
    default:
      var responseJson = jsonDecode(response.data.toString());
      // ignore: avoid_print
      print(responseJson);
      return responseJson;
  }
}
