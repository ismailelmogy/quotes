import 'dart:convert';
import 'package:http/http.dart' as http;
import 'custom_exception.dart';

dynamic handleHttpResponse(http.Response response) {
  switch (response.statusCode) {
    case 500:
      throw FetchDataException('Error occured while Communication with Server');
    default:
      var responseJson = jsonDecode(response.body.toString());
      // ignore: avoid_print
      print(responseJson);
      return responseJson;
  }
}
