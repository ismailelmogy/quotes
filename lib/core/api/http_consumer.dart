import 'api_consumer.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:async';
import 'custom_exception.dart';
import 'http_response.dart';

class HttpConsumer extends ApiConsumer {
  @override
  Future<dynamic> get(String url, {Map<String, String>? headers}) async {
    try {
      final response = await http.get(Uri.parse(url), headers: headers);
      return handleHttpResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
  }

  @override
  Future<dynamic> post(String url, {body, Map<String, String>? headers}) async {
    try {
      final response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: body,
      );
      return handleHttpResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
  }

  @override
  Future<dynamic> put(String url, {body, Map<String, String>? headers}) async {
    try {
      final response = await http.post(
        Uri.parse(url),
        headers: headers ,
        body: body,
      );
      return handleHttpResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
  }
}
