import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:quotesapp/config/locale/app_localizations.dart';
import 'package:quotesapp/core/utils/app_colors.dart';


class Commons {
  static Widget chuckyLoader() {
    return Center(child: SpinKitFoldingCube(
      itemBuilder: (BuildContext context, int index) {
        return DecoratedBox(
          decoration: BoxDecoration(
            color: index.isEven
                ? const Color(0xFFFFFFFF)
                : const Color(0xFF311433),
          ),
        );
      },
    ));
  }

  static void showError(BuildContext context, String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
              title: Text(
                message,
                style: const TextStyle(
                    color: Colors.black,
                    fontSize: 16
                     ),
              ),
              actions: <Widget>[
                TextButton(
                    child: Text(AppLocalizations.of(context)!.translate('ok')!),
                    style: TextButton.styleFrom(
                        primary: Colors.black,
                        textStyle: const TextStyle(
                            fontWeight: FontWeight.bold,
                         //   fontFamily: GoogleFonts.tajawal().fontFamily,
                            fontSize:16)),
                    onPressed: () => Navigator.of(context).pop()),
              ],
            ));
  }

  static Widget chuckyLoading(String message) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(padding: const EdgeInsets.all(10), child: Text(message)),
        chuckyLoader(),
      ],
    );
  }

  static void showToast(BuildContext context,
      {required String message, Color? color,
      ToastGravity? gravity}) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_LONG,
      gravity: gravity ?? ToastGravity.BOTTOM,
       backgroundColor:
       color ?? mainAppColor,
    );
  }
 
}
