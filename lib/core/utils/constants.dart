// urls
const baseUrl = "http://quotes.stormconsultancy.co.uk";
const randomQuoteUrl = baseUrl + "/random.json";

// Routes
const String initialRoute = '/';
const String mainRoute = '/home';


const String locale = 'locale';
const String english = 'en';
const String arabic = 'ar';
const String quotesScreenSubtitle = 'Your daily fuel 😎';
const String favQuotesScreenSubtitle = 'Your favorite quotes 😋';
const String cachedQuote = 'CACHED_QUOTE';
const String serverFailureMsg = 'Server Failure';
const String cacheFailureMsg = 'Cache Failure';
const String addFavSuccessMsg = 'Quote is Added to Favorite';
const String addFavFaliureMsg = 'Quote is already Added to Favorite';
const String deleteFavSuccessMsg = 'Quote is Deleted Successfully';
const String deleteFavFaliureMsg = 'Failed to Delete Quote';