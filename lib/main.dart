import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quotesapp/app.dart';
import 'package:quotesapp/core/storage/database_manager.dart';
import 'package:quotesapp/injection_container.dart' as di;
import 'package:quotesapp/injection_container.dart';
import 'package:responsive_builder/responsive_builder.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  DatabaseManager databaseManager = sl<DatabaseManager>();
  databaseManager.initializeDatabase();
  ResponsiveSizingConfig.instance.setCustomBreakpoints(
    const ScreenBreakpoints(desktop: 840, tablet: 600, watch: 200),
  );
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).then((_) {
    runApp(const AuotesApp());
  });
}
