part of 'theme_cubit.dart';

@immutable
abstract class ThemeState {
  final ThemeMode themeMode;
  const ThemeState(this.themeMode);
}

class SelectedThemeMode extends ThemeState {
  const SelectedThemeMode(ThemeMode themeMode) : super(themeMode);
}

