import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:quotesapp/config/themes/app_theme.dart';
part 'theme_state.dart';

class ThemeCubit extends Cubit<ThemeState> {
  ThemeCubit() : super(const SelectedThemeMode(ThemeMode.light));

  ThemeMode currentThemeMode = ThemeMode.light;

  Future<void> updateAppTheme(ThemeMode mode) async {
    currentThemeMode = mode;
    emit(SelectedThemeMode(currentThemeMode));
    AppTheme.setStatusBarAndNavigationBarColors(currentThemeMode);
  }
}
