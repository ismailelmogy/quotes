import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:tinycolor2/tinycolor2.dart';
import 'package:quotesapp/core/utils/app_colors.dart';
import 'package:quotesapp/core/utils/text_styles.dart';

class AppTheme {
  const AppTheme._();

  static final lightTheme = ThemeData(
    brightness: Brightness.light,
    fontFamily: textStyle.fontFamily,
    backgroundColor: whiteBackgroundColor.darken(4),
    scaffoldBackgroundColor: whiteBackgroundColor,
    primarySwatch: Colors.grey,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    iconTheme: IconThemeData(color: screenBackgroundColor),
    textTheme: TextTheme(
      bodyText1: textStyle.apply(
        color: screenBackgroundColor,
      ),
      bodyText2: textStyle.apply(
        color: screenBackgroundColor,
      ),
    ),
    colorScheme: ColorScheme.fromSwatch(
      primarySwatch: Colors.grey,
      brightness: Brightness.light,
    ).copyWith(
      secondary: accentColor,
    ),
  );

  static final darkTheme = ThemeData(
    brightness: Brightness.dark,
    fontFamily: textStyle.fontFamily,
    backgroundColor: screenBackgroundColor,
    scaffoldBackgroundColor: screenBackgroundColor,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    textTheme: TextTheme(
      bodyText1: textStyle.apply(
        color: Colors.white,
      ),
      bodyText2: textStyle.apply(
        color: Colors.white,
      ),
    ),
    iconTheme: const IconThemeData(
      color: Colors.white,
    ),
    colorScheme: ColorScheme.fromSwatch(
      primarySwatch: Colors.grey,
      brightness: Brightness.dark,
    ).copyWith(secondary: accentColor),
  );

  static Brightness get currentSystemBrightness =>
      SchedulerBinding.instance!.window.platformBrightness;

  static setStatusBarAndNavigationBarColors(ThemeMode themeMode) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: themeMode == ThemeMode.dark
          ? screenBackgroundColor
          : whiteBackgroundColor.darken(4),
    ));
  }
}
