import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quotesapp/core/utils/constants.dart';
import 'package:quotesapp/features/main/presentation/cubits/bottom_nav/bottom_nav_cubit.dart';
import 'package:quotesapp/features/main/presentation/screens/main_screen.dart';
import 'package:quotesapp/features/splash.dart';
import 'package:quotesapp/injection_container.dart';

class AppRoutes {
  static Route? onGenerateRoute(RouteSettings routeSettings) {
    // final args = routeSettings.arguments;

    switch (routeSettings.name) {
      case initialRoute:
        return MaterialPageRoute(builder: (_) => const SplashScreen());

      case mainRoute:
        return MaterialPageRoute(
          builder: (_) => 
          BlocProvider(
            create: (_) => sl<BottomNavCubit>(),
            child:
             const MainScreen(),
          ),
        );

      default:
        return null;
    }
  }
}
