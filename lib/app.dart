import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quotesapp/injection_container.dart' as di;
import 'config/locale/app_localizations_setup.dart';
import 'config/routes/app_routes.dart';
import 'config/themes/app_theme.dart';
import 'config/themes/cubit/theme_cubit.dart';
import 'features/main/presentation/cubits/locale/locale_cubit.dart';


class AuotesApp extends StatefulWidget {
  const AuotesApp({
    Key? key,
  }) : super(key: key);
  @override
  State<AuotesApp> createState() => _AuotesAppState();
}

class _AuotesAppState extends State<AuotesApp>  {


  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => di.sl<LocaleCubit>()..getSavedLang(),
        ),
        BlocProvider<ThemeCubit>(
          create: (_) => di.sl<ThemeCubit>(),
        ),
      ],
      child: BlocBuilder<LocaleCubit, LocaleState>(
        buildWhen: (previousState, currentState) =>
            previousState != currentState,
        builder: (_, localeState) {
       return   BlocBuilder<ThemeCubit, ThemeState>(
        buildWhen: (previousState, currentState) =>
            previousState != currentState,
        builder: (_, themeState) {
          return
           MaterialApp(
            title: 'Quotes',
            debugShowCheckedModeBanner: false,
            onGenerateRoute: AppRoutes.onGenerateRoute,
            theme: AppTheme.lightTheme,
            darkTheme: AppTheme.darkTheme,
           themeMode:themeState.themeMode,
            supportedLocales: AppLocalizationsSetup.supportedLocales,
            localizationsDelegates:
                AppLocalizationsSetup.localizationsDelegates,
            localeResolutionCallback:
                AppLocalizationsSetup.localeResolutionCallback,
            locale: localeState.locale,
          );
        },
      );
       },
      ),
    );
  }
}
