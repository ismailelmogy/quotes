import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:quotesapp/config/themes/cubit/theme_cubit.dart';
import 'package:quotesapp/core/network/network_info.dart';
import 'package:quotesapp/features/favorite_quotes/data/datasources/favorite_quotes_local_data_source.dart';
import 'package:quotesapp/features/favorite_quotes/domain/repositories/favorite_quotes_repository.dart';
import 'package:quotesapp/features/favorite_quotes/domain/usecases/delete_quote.dart';
import 'package:quotesapp/features/favorite_quotes/domain/usecases/like_quote.dart';
import 'package:quotesapp/features/favorite_quotes/presentation/cubit/favorite_quotes_cubit.dart';
import 'package:quotesapp/features/quotes/data/datasources/quote_local_data_source.dart';
import 'package:quotesapp/features/quotes/data/datasources/quote_remote_data_source.dart';
import 'package:quotesapp/features/quotes/data/repositories/quote_repository_impl.dart';
import 'package:quotesapp/features/quotes/domain/repositories/quote_repository.dart';
import 'package:quotesapp/features/quotes/domain/usecases/get_random_quote.dart';
import 'package:quotesapp/features/quotes/presentation/cubit/quote_cubit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'core/api/dio_consumer.dart';
import 'core/api/http_consumer.dart';
import 'core/storage/database_manager.dart';
import 'features/favorite_quotes/data/repositories/favorite_quotes_repository_impl.dart';
import 'features/favorite_quotes/domain/usecases/get_fav_quotes.dart';
import 'features/main/data/datasources/language_local_data_source.dart';
import 'features/main/data/datasources/language_local_data_source_impl.dart';
import 'features/main/data/repositories/language_repository_impl.dart';
import 'features/main/domain/repositories/language_repository.dart';
import 'features/main/domain/usecases/change_locale_use_case.dart';
import 'features/main/domain/usecases/get_saved_lang_use_case.dart';
import 'features/main/presentation/cubits/bottom_nav/bottom_nav_cubit.dart';
import 'features/main/presentation/cubits/locale/locale_cubit.dart';

final sl = GetIt.instance;
Future<void> init() async {
// Blocs
  sl.registerFactory<LocaleCubit>(
      () => LocaleCubit(changeLocaleUseCase: sl(), getSavedLangUseCase: sl()));
  sl.registerFactory<BottomNavCubit>(() => BottomNavCubit());
  sl.registerFactory<ThemeCubit>(() => ThemeCubit());
  sl.registerFactory(
    () => QuoteCubit(
      getRandomQuote: sl(),
    ),
  );
  sl.registerFactory(
    () => FavoriteQuotesCubit(likeQuote: sl(), deleteQuote: sl(),
    getFavQuotes: sl()),
  );

  // Use cases
  sl.registerLazySingleton<GetSavedLangUseCase>(
      () => GetSavedLangUseCase(languageRepository: sl()));
  sl.registerLazySingleton<ChangeLocaleUseCase>(
      () => ChangeLocaleUseCase(languageRepository: sl()));
  sl.registerLazySingleton(() => GetRandomQuote(
        quoteRepository: sl(),
      ));
  sl.registerLazySingleton(() => LikeQuote(
        favoriteQuotesRepository: sl(),
      ));
  sl.registerLazySingleton(() => DeleteQuote(
        favoriteQuotesRepository: sl(),
      ));
       sl.registerLazySingleton(() => GetFavQuotes(
        favQuotesRepository: sl(),
      ));


  // Repository
  sl.registerLazySingleton<LanguageRepository>(
      () => LanguageRepositoryImpl(languageLocalDataSource: sl()));
  sl.registerLazySingleton<QuoteRepository>(() => QuoteRepositoryImpl(
      networkInfo: sl(),
      quoteLocalDataSource: sl(),
      quoteRemoteDataSource: sl()));
  sl.registerLazySingleton<FavoriteQuotesRepository>(
      () => FavoriteQuotesRepositoryImpl(favoriteQuotesLocalDataSource: sl()));

  // Data sources
  sl.registerLazySingleton<LanguageLocalDataSource>(
    () => LanguageLocalDataSourceImpl(sharedPreferences: sl()),
  );
  sl.registerLazySingleton<QuoteLocalDataSource>(
    () => QuoteLocalDataSourceImpl(sharedPreferences: sl()),
  );
  sl.registerLazySingleton<QuoteRemoteDataSource>(
    () => QuoteRemoteDataSourceImpl(httpConsumer: sl()),
  );
  sl.registerLazySingleton<FavoriteQuotesLocalDataSource>(
    () => FavoriteQuoteLocalDataSourceImpl(databaseManager: sl()),
  );

  //Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => Dio());
  sl.registerLazySingleton(() => DatabaseManager());
  sl.registerLazySingleton(() => HttpConsumer());
  sl.registerLazySingleton(() => DioConsumer());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
